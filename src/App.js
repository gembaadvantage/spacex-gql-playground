import { gql, useQuery } from "@apollo/client";
import "./App.css";

function App() {
  const { loading, error, data } = useQuery(MISSIONS_QUERY);

  if (loading) {
    return null;
  }

  if (error) {
    return <p>Error :(</p>;
  }

  return (
    <div className="App">
      <h1>SpaceX Missions</h1>
      <ul>
        {data.missions.map((m, idx) => (
          <Mission key={idx} {...m} />
        ))}
      </ul>
    </div>
  );
}

const Mission = ({ name, description }) => (
  <li>
    <h3>{name}</h3>
    <p>{description}</p>
  </li>
);

const MISSIONS_QUERY = gql`
  query Missions {
    missions {
      name
      description
    }
  }
`;

export default App;
